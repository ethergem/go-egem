// Copyright 2015 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package params

import "gitlab.com/ethergem/go-egem/common"

// MainnetBootnodes are the enode URLs of the P2P bootstrap nodes running on
// the main Ethereum network.
var MainnetBootnodes = []string{
	// Egem Go Bootnodes
	"enode://0ee625f94d2e127821e566b2e9bb4842d8ad9a841a0906bef4c3694cabba03f76dd03eefa593eecb6f740a49d10401464407f11ca2767a72a03b4ec071b1c5f4@173.212.232.122:30666",
	"enode://f3e2de05fad0ec6e0951563ca9e8427fbc5469be57db028c1ddb93dcef686d1a43ebf7fd7ed050e939210a8f8c34e2e476b9a488d457c424c439e60aa365a263@62.171.132.13:30666",
	"enode://de0dfd16fec4004cf79ff3943f614e30013095596c420b9d09316f36948dfa37e12acc0671c0eeb09549aa170feca096eee82587edfb5063198ab65b95f1e0b1@81.166.105.8:30666",
	"enode://58b568cb4879fb827ab59206816d729975b96eab8f73490e120475410d428ea705068be7c35a84d2b64085dacf4a549647fa2ef806c02f22d0ec891978cb5fea@51.79.65.100:30300",
	"enode://9d7c21c0b13ae1b6320d7e44cbf7b60f8e6808ee538e5ca165a907a434e9d1352827da625260a63e007417449a029f4f35b658a24abc672525f03d24602c4953@51.79.65.100:30301",
	"enode://2eccc0a5f36bdc500f137219f57ffb649a380e076fbe66346bfef924eb917de7954dfcb5b6e10f15d6b00c3fba2ef5d608f6d7237e9a03d9aedf2db4ee446b44@192.248.181.19:30666",
	"enode://268c97667de6b333ceedcf67519673a87ee4901df73ecf2b0e625ef37cba9048fdf6314a215e64307e8ccbf0daf5a08e6d0463cf2efe8c6f25c7c40b5f0ecce9@194.163.160.48:30777",
	"enode://c358e77ada0730076815bec0092731d33a0f00a903819e9875e4d9e84c49e492bd83110bcdd8d2de90e4ed9a0aee7eb7fa4b5f67d3f4fdf1fc2e3df4fe9e924b@194.163.160.48:30666",
	"enode://f218096b17286af8e2f16bf727aeaf04358b47cb1b474f3c9788221e6525fcb750a70b5fa5ed278d42e4d994b70d6c6e768e063ed524ad09fadc035755f55c3d@45.76.249.83:30666",
	"enode://cc22b0c07cc0bc68e95ca5f129206bdeb8e6edcbe7978d60165cf8abe81d85cc750cd3f46757cf9894c808da6f6c2843c683a8e83ce62ba8aabb89c584fbc1a4@167.86.91.98:30666",
	"enode://f9e8ba9f69710deeb226c2c51f4a868e4e80752916daada85da628fabd3e7221f0e0fed65989a02909ce0735a86655e4ee80313e831c6a42e304732d0ec5ab6a@167.86.102.234:30666",
	"enode://1c212ff368198c018854d5738eafd89c8c467374d1c23a49872c596b14feefa8861fc60f2d5c502b88527a7d9f66eb0bb65e72f056c3377ae7f6fd6351b5d5b7@167.86.96.5:30666",
	"enode://9c7e8061cdc95bd50f11c8327acbd645f30c722683a70bf0e89698d5d957aa060ff71a73b1c7fc3009dd84aaa9dfb0e4ceee549070c1d8890f0cdb3d5fe1ce55@167.86.110.168:30666",
	"enode://00c3b503f626ffa382acf37a55265493205361c868d5c609e28ad5b78e1eceefa85e922008bc40062ecb0bbe6e9ed24841f6fbce207bb0a61206adac9b11b0eb@164.68.102.160:30666",
}

// RopstenBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Ropsten test network.
var RopstenBootnodes = []string{
}

// RinkebyBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Rinkeby test network.
var RinkebyBootnodes = []string{
}

// GoerliBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Görli test network.
var GoerliBootnodes = []string{
}

// YoloV2Bootnodes are the enode URLs of the P2P bootstrap nodes running on the
// YOLOv2 ephemeral test network.
var YoloV2Bootnodes = []string{
}

const dnsPrefix = "enrtree://AKA3AM6LPBYEUDMVNU3BSVQJ5AD45Y7YPOHJLEF6W26QOE4VTUDPE@"

// KnownDNSNetwork returns the address of a public DNS-based node list for the given
// genesis hash and protocol. See https://github.com/ethereum/discv4-dns-lists for more
// information.
func KnownDNSNetwork(genesis common.Hash, protocol string) string {
	var net string
	switch genesis {
	case MainnetGenesisHash:
		net = "mainnet"
	case RopstenGenesisHash:
		net = "ropsten"
	case RinkebyGenesisHash:
		net = "rinkeby"
	case GoerliGenesisHash:
		net = "goerli"
	default:
		return ""
	}
	return dnsPrefix + protocol + "." + net + ".ethdisco.net"
}
